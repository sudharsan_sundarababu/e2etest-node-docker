Feature: Current Weather

   As a user in the internet,
   I want to read current weather of my location,
   so that I can plan my travel and other outdoor activities.

@smoke-test
Scenario Outline: Read current weather by location
Given a user in the internet
When I read current weather for the location '<location>'
Then I should be able to view weather summary with no cost implication
And I should be able to read weather deatils
Examples:
| location     |
| London       |
| Paris        |
| Tokyo        |

@smoke-test
Scenario Outline: Read current weather by country specific location
Given a user in the internet
When I read current weather for the location '<location>' in the country '<country>'
Then I should be able to view weather summary with no cost implication
And I should be able to read weather deatils
Examples:
| location     | country     |
| London       | UK          |


@error-handling-test
Scenario: Read current weather of an invalid location
Given a user in the internet
When I read current weather for the location 'invalidLocation'
Then I should receive default weather summary for London
