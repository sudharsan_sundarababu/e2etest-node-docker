const assert = require('assert');
const {
  Given,
  When,
  Then
} = require('cucumber');
const { setDefaultTimeout } = require('cucumber');

setDefaultTimeout(60 * 1000);


const { createUser } = require('../lib/user/User');
const { OK } = require('../config');

Given('a user in the internet', async function () {
  this.user = await createUser();
});

When('I read current weather for the location {string}', async function (location) {
  this.weatherSummary = await this.user.readWeather(location);
});

When('I read current weather for the location {string} in the country {string}', async function (location, country) {
  this.weatherSummary = await this.user.readWeather(location, country);
});

Then('I should be able to view weather summary with no cost implication', function () {
  assert.equal(this.weatherSummary.status, OK);
});

Then('I should be able to read weather deatils', function () {
  assert(this.weatherSummary.data.weather, 'Weather details are missing in the weather summary');
});


Then('I should receive default weather summary for London', function () {
  assert.equal(this.weatherSummary.status, OK);
});
