# End to End tests

This repository contains automated scripts that validate end to end behaviour of weather information system from ```https://openweathermap.org```

Features are defined in ```Gherkin``` language. Gherkin statements are interpreted using node module ```cucucmber```. Tests and libraries are implemented in ```node.js```.

# Features
The following features are tested
  - Current weather information.
  - Historic weather data.

# Installation
> If you choose to run test using docker skip this step and run test using option-2.

It is recommended to have [Node.js](https://nodejs.org/) V10+.

Test can be installed in the repository home using the command
```
$ npm install
```

# Run test
Option-1 : Run using local node runtime
```
$ ./node_modules/.bin/cucumber.js ./features
```
Option-2 : Run using docker
```
$ docker build -t e2e .
$ docker run e2e
```

# License
ISC