const isOnline = require('is-online');
const uuidv1 = require('uuid/v1');

const { currentWeather } = require('../api/currentWeatherApi');

class User {

  constructor() {
    this.userId = uuidv1();
  }

  readWeather(location, country = undefined) {
    console.log(`-> User ${this.userId} reads weather information`);
    return currentWeather(location, country);
  }
}

const createUser = () => {
  const user = new User();
  return isOnline()
    .then(() => Promise.resolve(user))
    .catch(() => Promise.reject(new Error('User does not have internet connection.\n Please check his internet connection and test again.')));
};

module.exports = { createUser };
