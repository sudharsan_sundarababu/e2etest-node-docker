const axios = require('axios');

const { apiKey } = require('../../config');

const currentWeather = (location, country) => {
  let url = `https://samples.openweathermap.org/data/2.5/weather?q=${location}&appid=${apiKey}`;

  if (country) url = `https://samples.openweathermap.org/data/2.5/weather?q=${location},${country}&appid=${apiKey}`;

  console.log(`-> Request url : ${url}`);
  return axios.get(url)
    .then((response) => {
      console.log('-> Successfully received response');
      return response;
    })
    .catch((err) => {
      console.log(`-> Failure response ${err}`);
      return Promise.reject(err);
    });
};

module.exports = {
  currentWeather,
};
