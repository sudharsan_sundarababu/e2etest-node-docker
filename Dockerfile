FROM node:10.0.0 as base

ENV RUN_DIR = /opt/e2e/

# Build image
FROM base
# USER 1001
WORKDIR $RUN_DIR
COPY . .
RUN npm install
CMD [ "npm", "start" ]

